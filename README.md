# M8110 Partial Differential Equations

This book contains the organization information along with a short summary of the course M8110 Partial Differential Equations taught in the Fall 2023 semester on SCI MUNI, Department of Mathematics and Statistics by assoc. prof. Veselý.

> The entirety of these notes is in Czech.

## Notice

As these are only notes/summary extracted from the course material, I claim only the right to the typesetted material itself and not the selection or style of presentation of the content.

In this repository, you may also find assoc. prof. Veselý's textbook, which **I claim no right to**.